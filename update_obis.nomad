variable "REGISTRY" {
  type        = string
  description = "The name of the GCR repository."
}

variable "REGISTRY_USERNAME" {
  type        = string
  description = "The username needed to authenticate to GCR to pull OBIS images."
}

variable "REGISTRY_PASSWORD" {
  type        = string
  description = "The password needed to authenticate to GCR to pull OBIS images."
}

variable "UPDATE_OBIS_REGISTRY_REPO" {
  type        = string
  description = "The repository within GCR where the docker image exists."
}

variable "UPDATE_OBIS_IMAGE_NAME" {
  type        = string
  description = "The docker image used for the task."
}

job "update_obis" {
  datacenters = ["dc1"]
  type        = "service"
  
  update {
    max_parallel      = 1
    min_healthy_time  = "10s"
    healthy_deadline  = "2m"
    progress_deadline = "5m"
    auto_revert       = true
    canary            = 2
  }

  migrate {
    max_parallel     = 1
    health_check     = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "3m"
  }

  group "update_obis" {
    count = 1

    network {
      mode = "bridge"

      port "update_obis" {
        static = 8000
        to     = 8000
      }
    }

    restart {
      attempts = 2
      interval = "5m"
      delay    = "15s"
      mode     = "fail"
    }

    task "update_obis" {
      driver = "docker"

      config {
        image = join("/", [var.REGISTRY, var.UPDATE_OBIS_REGISTRY_REPO, var.UPDATE_OBIS_IMAGE_NAME])
        force_pull = true
        ports = ["update_obis"]

        auth {
          server_address = var.REGISTRY
          username       = var.REGISTRY_USERNAME
          password       = var.REGISTRY_PASSWORD
        }

        mount {
          type     = "bind"
          source   = "/opt/obis/pipes/"
          target   = "/hostpipe/"
          readonly = false

          bind_options {
            propagation = "rshared"
          }
        }
      }

      resources {
        cpu    = 100
        memory = 128
      }
    }
  }
}
