variable "REGISTRY" {
  type        = string
  description = "The name of the GCR repository."
}

variable "REGISTRY_USERNAME" {
  type        = string
  description = "The username needed to authenticate to GCR to pull OBIS images."
}

variable "REGISTRY_PASSWORD" {
  type        = string
  description = "The password needed to authenticate to GCR to pull OBIS images."
}

variable "REGISTRY_REPO" {
  type        = string
  description = "The repository within GCR where the docker image exists."
}

variable "API_IMAGE_NAME" {
  type        = string
  description = "The docker image used for the task."
}

variable "ANGULAR_IMAGE_NAME" {
  type        = string
  description = "The docker iamge used for the task."
}

job "obis" {
  datacenters = ["dc1"]
  type        = "service"
  
  update {
    max_parallel      = 1
    min_healthy_time  = "10s"
    healthy_deadline  = "2m"
    progress_deadline = "5m"
    auto_revert       = true
    canary            = 2
  }

  migrate {
    max_parallel     = 1
    health_check     = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "3m"
  }

  group "obis" {
    count = 1

    scaling {
      enabled = true
      min     = 1
      max     = 3
    }

    network {
      mode = "bridge"

      port "obis_api" {
        static = 8080
        to     = 8080
      }

      port "obis_angular" {
        static = 80
        to     = 80
      }
    }

    restart {
      attempts = 2
      interval = "5m"
      delay    = "15s"
      mode     = "fail"
    }

    task "obis_api" {
      driver = "docker"

      config {
        image = join("/", [var.REGISTRY, var.REGISTRY_REPO, var.API_IMAGE_NAME])
        force_pull = true
        ports = ["obis_api"]

        auth {
          server_address = var.REGISTRY
          username       = var.REGISTRY_USERNAME
          password       = var.REGISTRY_PASSWORD
        }

        mount {
          type     = "bind"
          source   = "/opt/obis/data/db.sqlite3"
          target   = "/usr/src/app/db.sqlite3"
          readonly = false

          bind_options {
            propagation = "rshared"
          }
        }
        
        mount {
          type     = "bind"
          source   = "/opt/obis/log/api.log"
          target   = "/usr/src/app/log/api.log"
          readonly = false

          bind_options {
            propagation = "rshared"
          }
        }

        mount {
          type     = "bind"
          source   = "/opt/obis/config/api.env"
          target   = "/usr/src/app/api/.env"
          readonly = false

          bind_options {
            propagation = "rshared"
          }
        }
      }

      resources {
        cpu    = 450
        memory = 256
      }
    }

    task "obis_angular" {
      driver = "docker"

      config {
        image = join("/", [var.REGISTRY, var.REGISTRY_REPO, var.ANGULAR_IMAGE_NAME])
        force_pull = true
        ports = ["obis_angular"]

        auth {
          server_address = var.REGISTRY
          username       = var.REGISTRY_USERNAME
          password       = var.REGISTRY_PASSWORD
        }

        mount {
          type     = "bind"
          source   = "/opt/obis/static/"
          target   = "/var/www/html/static/"
          readonly = false

          bind_options {
            propagation = "rshared"
          }
        }

        mount {
          type     = "bind"
          source   = "/opt/obis/config/db-query.ini"
          target   = "/var/www/private_config/db-query.ini"
          readonly = false

          bind_options {
            propagation = "rshared"
          }
        }

        mount {
          type     = "bind"
          source   = "/opt/obis/config/obis_ou_edu.pem"
          target   = "/ssl/obis_ou_edu.pem"
          readonly = false

          bind_options {
            propagation = "rshared"
          }
        }

        mount {
          type     = "bind"
          source   = "/opt/obis/config/obis_ou_edu.key"
          target   = "/ssl/obis_ou_edu.key"
          readonly = false

          bind_options {
            propagation = "rshared"
          }
        }

        mount {
          type     = "bind"
          source   = "/opt/obis/config/apache2.conf"
          target   = "/etc/apache2/apache2.conf"
          readonly = false

          bind_options {
            propagation = "rshared"
          }
        }

        mount {
          type     = "bind"
          source   = "/opt/obis/config/obis.conf"
          target   = "/etc/apache2/sites-enabled/000-default.conf"
          readonly = false

          bind_options {
            propagation = "rshared"
          }
        }
      }

      resources {
        cpu    = 450
        memory = 256
      }
    }
  }
}
